package movieRental.core.service;

import movieRental.core.model.Client;

import java.util.List;

public interface ClientServiceInterface extends BaseServiceInterface<Client, Long> {

    List<Client> filterBy(Client example);
    List<Client> filter(String value);
    List<Client> filterBySecondName(String value);
    List<Client> filterByJob(String value);
    List<Client> filterByAge(int age);
    List<Client> getAllSortedDescendingByFields(String... fields);
    List<Client> getAllSortedAscendingByFields(String... fields);
}
