package controller;

import converter.BaseConverter;
import dto.ClientDto;
import dto.ClientsDto;
import movieRental.core.model.Client;
import movieRental.core.service.ClientService;
import movieRental.core.service.ClientServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@RestController
public class ClientController {

    @Autowired
    private ClientServiceInterface clientService;

    @Autowired
    private BaseConverter<Client, ClientDto> clientConverter;

    public static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    List<ClientDto> getClients(){
        log.trace("getAll --- method entered");
        log.trace("getAll --- method ended");
        List<Client> students = clientService.getAll();
        return new ArrayList<>(clientConverter.convertModelsToDtos(students));
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto save(@RequestBody ClientDto clientDto){
        log.trace("save --- method entered");
        log.trace("save --- method ended");
        return clientConverter.convertModelToDto(clientService.save(
                clientConverter.convertDtoToModel(clientDto)));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto update(@PathVariable Long id,@RequestBody ClientDto clientDto){
        log.trace("update --- method entered");
        log.trace("update --- method ended");
        return  clientConverter.convertModelToDto(clientService.update(id,
                clientConverter.convertDtoToModel(clientDto)));
    }

    @RequestMapping(value = "/clients/delete/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> delete(@PathVariable Long id){
        log.trace("delete --- method entered");
        log.trace("delete --- method ended");
        if (clientService.deleteById(id))
            return new ResponseEntity<>(HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/clients/filter", method  = RequestMethod.GET)
    List<ClientDto> filterBy(@RequestParam Optional<String> firstName, @RequestParam Optional<String> secondName, @RequestParam Optional<String> job, @RequestParam Optional<Integer> age)
    {
        log.trace("filterBy - method entered: firstName={}, secondName={}, job={}, age={}", firstName, secondName, job, age);

        Client client = new Client();

        firstName.ifPresent(client::setFirstname);
        secondName.ifPresent(client::setSecondname);
        job.ifPresent(client::setJob);
        age.ifPresent(client::setAge);

        return new ArrayList<>(clientConverter.convertModelsToDtos(clientService.filterBy(client)));
    }

    @RequestMapping(value = "/clients/firstname/{name}", method = RequestMethod.GET)
    ClientsDto filter(@PathVariable String name){
        log.trace("filter --- method entered");
        log.trace("filter --- method ended");
        return new ClientsDto(clientConverter.
                convertModelsToDtos(clientService.filter(name)));
    }

    @RequestMapping(value = "/clients/secondname/{name}", method = RequestMethod.GET)
    ClientsDto filterBySecond(@PathVariable String name){
        log.trace("filterBySecond --- method entered");
        log.trace("filterBySecond --- method ended");
        return new ClientsDto(clientConverter.
                convertModelsToDtos(clientService.filterBySecondName(name)));
    }

    @RequestMapping(value = "/clients/job/{job}", method = RequestMethod.GET)
    ClientsDto filterByJob(@PathVariable String job){
        log.trace("filterByJob --- method entered");
        log.trace("filterByJob --- method ended");
        return new ClientsDto(clientConverter.
                convertModelsToDtos(clientService.filterByJob(job)));
    }

    @RequestMapping(value = "/clients/age/{age}", method = RequestMethod.GET)
    ClientsDto filterByAge(@PathVariable int age){
        log.trace("filterByAge --- method entered");
        log.trace("filterByAge --- method ended");
        return new ClientsDto(clientConverter.
                convertModelsToDtos(clientService.filterByAge(age)));
    }

    @RequestMapping(value = "/clients/asc/{fields}", method = RequestMethod.GET)
    ClientsDto displayAsc(@PathVariable String fields){
        log.trace("displayAsc --- method entered");
        log.trace("displayAsc --- method ended");
        return new ClientsDto(clientConverter.
                convertModelsToDtos(clientService.getAllSortedAscendingByFields(fields)));
    }

    @RequestMapping(value = "/clients/desc/{fields}", method = RequestMethod.GET)
    ClientsDto displayDesc(@PathVariable String fields){
        log.trace("displayDesc --- method entered");
        log.trace("displayDesc --- method ended");
        return new ClientsDto(clientConverter.
                convertModelsToDtos(clientService.getAllSortedDescendingByFields(fields)));
    }
}
