package controller;

import converter.BaseConverter;
import dto.ClientsDto;
import dto.MovieDto;
import dto.MoviesDto;
import movieRental.core.model.Movie;
import movieRental.core.service.MovieServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MovieController {
    @Autowired
    private MovieServiceInterface movieService;

    @Autowired
    private BaseConverter<Movie, MovieDto> movieConverter;

    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    List<MovieDto> getAll(){
        log.trace("getAll --- method entered");
        log.trace("getAll --- method ended");
        return new ArrayList<>(movieConverter.
                convertModelsToDtos(movieService.getAll()));
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    MovieDto save(@RequestBody MovieDto movieDto){
        log.trace("save --- method entered");
        log.trace("save --- method ended");
        return movieConverter.convertModelToDto(movieService.save(
                movieConverter.convertDtoToModel(movieDto)));
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.PUT)
    MovieDto update(@PathVariable Long id, @RequestBody MovieDto movieDto){
        log.trace("update --- method entered");
        log.trace("update --- method ended");
        return movieConverter.convertModelToDto(movieService.update(id,
                movieConverter.convertDtoToModel(movieDto)));
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> delete(@PathVariable Long id){
        log.trace("delete --- method entered");
        log.trace("delete --- method ended");
        if (movieService.deleteById(id))
            return new ResponseEntity<>(HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/movies/title/{name}", method = RequestMethod.GET)
    MoviesDto filter(@PathVariable String name){
        log.trace("filter --- method entered");
        log.trace("filter --- method ended");
        return new MoviesDto(movieConverter.
                convertModelsToDtos(movieService.filter(name)));
    }

    @RequestMapping(value = "/movies/description/{name}", method = RequestMethod.GET)
    MoviesDto filterByCategory(@PathVariable String name){
        log.trace("filterByCategory --- method entered");
        log.trace("filterByCategory --- method ended");
        return new MoviesDto(movieConverter.
                convertModelsToDtos(movieService.filterByDescription(name)));
    }

    @RequestMapping(value = "/movies/price/{price}", method = RequestMethod.GET)
    MoviesDto filterByPrice(@PathVariable int price){
        log.trace("filterByPrice --- method entered");
        log.trace("filterByPrice --- method ended");
        return new MoviesDto(movieConverter.
                convertModelsToDtos(movieService.filterByPrice(price)));
    }

    @RequestMapping(value = "/movies/rating/{rating}", method = RequestMethod.GET)
    MoviesDto filterByRating(@PathVariable int rating){
        log.trace("filterByRating --- method entered");
        log.trace("filterByRating --- method ended");
        return new MoviesDto(movieConverter.
                convertModelsToDtos(movieService.filterByRating(rating)));
    }

    @RequestMapping(value = "/movies/asc/{fields}", method = RequestMethod.GET)
    MoviesDto displayAsc(@PathVariable String fields){
        log.trace("displayAsc --- method entered");
        log.trace("displayAsc --- method ended");
        return new MoviesDto(movieConverter.
                convertModelsToDtos(movieService.getAllSortedAscendingByFields(fields)));
    }

    @RequestMapping(value = "/movies/desc/{fields}", method = RequestMethod.GET)
    MoviesDto displayDesc(@PathVariable String fields){
        log.trace("displayDesc --- method entered");
        log.trace("displayDesc --- method ended");
        return new MoviesDto(movieConverter.
                convertModelsToDtos(movieService.getAllSortedDescendingByFields(fields)));
    }
}
