import {Component} from "@angular/core";
import {RentalService} from "./shared/rental.service";

@Component({
    moduleId: module.id,
    selector: 'ubb-rentals',
    templateUrl: './rentals.component.html',
    styleUrls: ['./rentals.component.css'],
})

export class RentalsComponent {

}
