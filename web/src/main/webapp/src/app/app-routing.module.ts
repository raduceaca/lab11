import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientsComponent} from "./client/clients.component";
import {ClientDetailComponent} from "./client/client-detail/client-detail.component";
import {MoviesComponent} from "./movie/movies.component";
import {MovieDetailComponent} from "./movie/movie-detail/movie-detail.component";
import {RentalsComponent} from "./rental/rentals.component";
import {RentalDetailComponent} from "./rental/rental-detail/rental-detail.component";

const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'clients', component: ClientsComponent},
  {path: 'client/detail/:id', component: ClientDetailComponent},
  {path: 'movies', component: MoviesComponent},
  {path: 'movie/detail/:id', component: MovieDetailComponent},
  {path: 'rentals', component: RentalsComponent},
  {path: 'rental/detail/:id', component: RentalDetailComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
