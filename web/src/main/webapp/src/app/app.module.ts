import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {ClientDetailComponent} from "./client/client-detail/client-detail.component";
import {ClientsComponent} from "./client/clients.component";
import {ClientListComponent} from "./client/client-list/client-list.component";
import {ClientService} from "./client/shared/client.service";

import {MovieDetailComponent} from "./movie/movie-detail/movie-detail.component";
import {MoviesComponent} from "./movie/movies.component";
import {MovieListComponent} from "./movie/movie-list/movie-list.component";
import {MovieService} from "./movie/shared/movie.service";

import {RentalDetailComponent} from "./rental/rental-detail/rental-detail.component";
import {RentalsComponent} from "./rental/rentals.component";
import {RentalListComponent} from "./rental/rental-list/rental-list.component";
import {RentalService} from "./rental/shared/rental.service";

@NgModule({
  declarations: [
    AppComponent,
    ClientDetailComponent,
    ClientsComponent,
    ClientListComponent,
    MovieDetailComponent,
    MoviesComponent,
    MovieListComponent,
    RentalDetailComponent,
    RentalsComponent,
    RentalListComponent,



  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [ClientService, MovieService, RentalService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
